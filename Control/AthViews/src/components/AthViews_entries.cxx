#include "../ViewDataVerifier.h"
#include "../ViewTestAlg.h"
#include "../MinimalViewAlg.h"
#include "../AliasOutOfView.h"
#include "../RoiCollectionToViews.h"

DECLARE_COMPONENT( AthViews::ViewDataVerifier )
DECLARE_COMPONENT( AthViews::ViewTestAlg )
DECLARE_COMPONENT( AthViews::MinimalViewAlg )
DECLARE_COMPONENT( AthViews::AliasOutOfView )
DECLARE_COMPONENT( AthViews::RoiCollectionToViews )

