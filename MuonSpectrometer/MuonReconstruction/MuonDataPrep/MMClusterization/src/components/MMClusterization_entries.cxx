#include "../SimpleMMClusterBuilderTool.h"
#include "../UTPCMMClusterBuilderTool.h"
#include "../ProjectionMMClusterBuilderTool.h"
#include "../ConstraintAngleMMClusterBuilderTool.h"


DECLARE_COMPONENT(Muon::SimpleMMClusterBuilderTool)
DECLARE_COMPONENT(Muon::UTPCMMClusterBuilderTool)
DECLARE_COMPONENT(Muon::ProjectionMMClusterBuilderTool)
DECLARE_COMPONENT(Muon::ConstraintAngleMMClusterBuilderTool)

