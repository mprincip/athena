#include "../TrigTileRODMuAlg.h"
#include "../TrigTileLookForMuAlg.h"
#include "../TrigTileMuToNtuple.h"
#include "../TrigTileMuFex.h"

DECLARE_COMPONENT( TrigTileRODMuAlg )
DECLARE_COMPONENT( TrigTileLookForMuAlg )
DECLARE_COMPONENT( TrigTileMuToNtuple )
DECLARE_COMPONENT( TrigTileMuFex )
